import logo from "./logo.svg";
import "./App.css";
import Header from "./Ex_layout/Header";
import Welcome from "./Ex_layout/Welcome";
import Content from "./Ex_layout/Content";
import Footer from "./Ex_layout/Footer";

function App() {
  return (
    <div className="App">
      <Header />
      <Welcome />
      <Content />
      <Footer />
    </div>
  );
}

export default App;
